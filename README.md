# st4_mds_prepa

Preparation for Etude de cas ST4 dominante MDS

## Installation

Go to path

`cd /path/to/st4_mds_prepa`

Create a virtual environment (you may skip this step if mds_env already exists in your directory)

`virtualenv -p python3 mds_env`

Activate virtualenv

`. mds_env/bin/activate`

Install requirements

`pip install -r requirements.txt`

### Adding virtualenv to jupyter notebook

Install jupyter
`(mds-env)$ pip install jupyter`

Add kernel to jupyter
`(mds-env)$ ipython kernel install --name mds-env --user`
